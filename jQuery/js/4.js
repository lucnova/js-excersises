'use strict';
/* -------------------------------------------------------------------------- */
/*                               EFECTOS DEL DOM                              */
/* -------------------------------------------------------------------------- */

$(document).ready(function () {
    let boxSelect = $("#myBox");
    let buttonShowSelect = $("#buttonShow");
    let buttonHideSelect = $("#buttonHide");
    let buttonShowHideSelect = $("#buttonShowHide");
    let buttonAnimSelect = $("#buttonAnim");

    /* ---------------------------- Mostrar / Ocultar --------------------------- */

    buttonShowSelect.hide();

    buttonShowSelect.click(function (e) {
        e.preventDefault();

        buttonShowSelect.hide();
        buttonHideSelect.show();
        //boxSelect.show('fast');    // Mostrar con efecto
        boxSelect.fadeIn('fast', () => {
            console.log("Pop");
        }); // Aparecer lentamente
        //boxSelect.fadeTo('fast', 0.8);     // Desvanecer a cierto grado de opacidad
    });

    buttonHideSelect.click(function (e) {
        e.preventDefault();

        buttonShowSelect.show();
        buttonHideSelect.hide();
        //boxSelect.hide('fast');    // Ocultar con efecto
        boxSelect.fadeOut('fast', () => {
            console.log("Woosh");
        }); // Desaparecer lentamente
        //boxSelect.fadeTo('fast', 0.2);     // Desvanecer a cierto grado de opacidad
    });

    buttonShowHideSelect.click(function (e) {
        e.preventDefault();

        //boxSelect.toggle('fast');    // Igual a un show/hide
        //boxSelect.fadeToggle('fast');    // Igual a un fadeIn/FadeOut
        boxSelect.slideToggle('fast', () => {
            console.log("Sliiiiide");
        }); // Igual a un movimiento slide
    });

    /* ------------------------- Animación personalizada ------------------------ */

    buttonAnimSelect.click(function (e) {
        e.preventDefault();

        // Hace el cambio de estilo animado
        boxSelect
        .animate({
            marginLeft: '256px'
        }, 'slow')
        .animate({
            height: '160px'
        }, 'slow')
        .animate({
            marginLeft: '0px'
        }, 'slow');

        // Se puede concatenar animaciones para que sean ejecutadas en "pipeline"
    });
});
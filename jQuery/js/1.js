'use strict';
/* -------------------------------------------------------------------------- */
/*                                   JQUERY                                   */
/* -------------------------------------------------------------------------- */
/**
 * Que veré:
 *  - Información general sobre jQuery
 *  - Selectores de jQuery; más sencillo que JS puro.
 *      Seleccionar elementos del DOM.
 *  - Eventos;  existen eventos en jQuery también.
 *  - Efectos y Animaciones.
 *  - AJAX; para las peticiones de datos.
 *  - Plugin jQuery UI: 
 *      Manipular la interfaz de una web más extensa.
 *      Animaciones
 *      Plugins y Widgets
 */

/**
 * jQuery reduce muchisimo código, y las interacciones ya son
 * más sencillas en proyectos grandes.
 * Se usa muchísimo en Proyectos Monolíticos: 
 *          (BackEnd + FrontEnd en el mismo lugar) 
 *          Como Laravel
 *          - Son páginas de internet comúnes.
 * 
 * Para proyectos SPA: 
 *      (con backend y frontend separados, FrontEnd consume BackEnd con APIS)
 *      Angular ayuda bastante más.
 *       - Son aplicaciones.
 */

/**
 *      Notas:
 *      - $ es alias de: jQuery. (  $(document)   ===   jQuery(document)  )
 *      - jQuery es un objeto.
 *      - $(X): X es un elemento seleccionado.
 *      - $(X).Y();   Y() es un método llamado al selector.
 *
 * 
 *      A veces usando funciones flecha, los eventos no funcionan correctamente.
 *          al tratar de usar (this).
 */

$(document).ready(function () {
    console.log("jQuery Ready.");
    /* -------------------------------------------------------------------------- */
    /*                             TODO TU CODIGO AQUI                            */
    /* -------------------------------------------------------------------------- */

    /* ---------------------------- SELECTORES DE ID ---------------------------- */

    // Seleccionar elemento título.
    // El método .css(clave, valor) modifica las propiedades CSS de un elemento dado.
    const myTitle = $("#myTitle").css("background", "red")
        .css("color", "white");

    // Se puede concatenar las llamadas a .css en vez de hacer llamadas.
    //  pues hacen referencia al mismo objeto seleccionado.
    console.log("Seleccion: ", myTitle, typeof myTitle);

    $("#myText").css("background", "blue")
        .css("color", "white");

    /* --------------------------- SELECTORES DE CLASE -------------------------- */

    const myWhiteElements = $(".white");
    $(".white").css("background", "white")
        .css("color", "black");

    const myBlackElements = $(".black");
    $(".black").css("background", "black")
        .css("color", "white");

    console.log("Seleccion: ", myWhiteElements, typeof myWhiteElements);
    console.log("Seleccion: ", myBlackElements, typeof myBlackElements);

    /* ------------------------- SELECTORES DE ETIQUETA ------------------------- */

    $("h1").click(function () {
        console.log("Hey!");
        $(this).toggleClass("highlight");
    });

    /* ------------------------- SELECTORES DE ATRIBUTO ------------------------- */

    $("[title]").css("background", "purple")
        .css("color", "white");
    $("[title='MDN']").css("background", "orange")
        .css("color", "black");

    /* ---------------------------- SELECTOR MULTILPE --------------------------- */
    $("p, li").css("margin-top", "32px");

    /* -------------------------------- BUSQUEDAS ------------------------------- */
    // Evitar hacerlas.
    console.log("*** Busqueda en myLinks por atributo title=MDN ***");
    let mySearch = $("#myLinks").find("[title=MDN]");
    console.log(mySearch);

    console.log("*** Busqueda en myTexts por atributo title=MDN ***");
    mySearch = $("#myTexts").find("[title=MDN]");
    console.log(mySearch);

    console.log("*** Busqueda en Listado.parent() por atributo title=MDN ***");
    mySearch = $("#myLinksList").parent().find("[title=MDN]");
    console.log(mySearch);

    console.log("*** Busqueda SIN find title=MDN ***");
    // Hacerlas así.
    mySearch = $("#myLinks [title=MDN]");
    console.log(mySearch);
});
'use strict';

/* -------------------------------------------------------------------------- */
/*                                   EVENTOS                                  */
/* -------------------------------------------------------------------------- */

$(document).ready(function () {
    console.log("jQuery Ready.");

    /* ------------------------------ Evento Hover ------------------------------ */

    /* Combina: El MouseOver y el MouseOut:
     *   $(selection).mouseover();
     */
    /*      Recibe dos funciones, una de Over y otra de Out*/
    $("#myBox").hover(function () {
        // Over
        $(this).css("background", "red");
    }, function () {
        // Out
        $(this).css("background", "black");
    });

    /* ------------------------------ Evento Click ------------------------------ */

    $("#myBox").click(function () {
        $(this).css("background", "blue");
    });

    /* --------------------------- Evento Double Click -------------------------- */

    $("#myBox").dblclick(function () {
        $(this).css("background", "green");
    });

    /* ------------------------------ Evento Focus ------------------------------ */
    // Foco dentro del elemento
    $("#myInputA").focus(function (e) {
        e.preventDefault(); // Llamada al evento; evitar que se ejecute acción Default
        $(this).css("border", "4px solid green");
    });

    /* ------------------------------- Evento Blur ------------------------------ */
    // Foco fuera del elemento
    $("#myInputA").blur(function (e) {
        e.preventDefault(); // Llamada al evento; evitar que se ejecute acción Default
        $(this).css("border", "4px solid gray");

        $("#myBox").text($(this).val());
    });

    /* ------------------------------- Mouse Move ------------------------------- */
    $(document).mousemove(function (e) {
        // Ocultar Cursor
        //$("body").css("cursor", "none");
        // Valores: e.clientX, e.clientY, e.pageX, e.pageY
        // Mostrar coordenadas en consola 
        //console.clear();
        //console.log(e.clientX, e.clientY);
        $("#navi").css("left", e.pageX)
            .css("top", e.pageY);
    });

    /**
     * OTROS EVENTOS:
     *      Mouse Down y Mouse Up
     */
});
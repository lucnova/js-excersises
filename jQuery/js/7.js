'use strict';
/* -------------------------------------------------------------------------- */
/*                                  jQuery UI                                 */
/* -------------------------------------------------------------------------- */

/**
 *      Libreria para jQuery, añade funcionalidades amplias a interfaces.
 * 
 */

$(document).ready(function () {
    // Arrastrar elemento
    $(".myNote").draggable();

    // Modificar tamaño
    $(".myNote").resizable();

    // Elemento seleccionable
    //$("#myList").selectable();

    // Elemento ordenable
    $("#myList").sortable({
        update: (event, ui) => {
            console.log("Item changed");
            //console.log(event, typeof event);
            //console.log(ui, typeof ui);
        }
    });

    $(".myBun").draggable();

    $(".myBurger").droppable({
        drop: () => {
            console.log("Yum!");
            alert("Yum!");
        }
    });
});
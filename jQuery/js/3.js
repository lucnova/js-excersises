'use strict';

/* -------------------------------------------------------------------------- */
/*                            MANIPULACION DEL DOM                            */
/* -------------------------------------------------------------------------- */

$(document).ready(function () {
    function removeHref(elementToRemove) {
        const idToRemove = $(elementToRemove).attr("id");  // Obtengo el ID del botón clickeado.
        const elementToRemoveAttr = $("a").eq(idToRemove);  // Selecciono elemento

        //console.log($("a"));
        //console.log(idToRemove, elementToRemoveAttr, typeof elementToRemoveAttr);
        
        /* Remover atributo */
        elementToRemoveAttr.removeAttr("href");
    }


    /* ITERAR SOBRE ELEMENTOS DEL DOM */
    console.log("Cantidad de Elementos <a>:", $("a").length);
    $("a").each(function (index, element) {
        // element == this
        console.log(index, element, typeof element);
        // Cambiar el texto por un link
        element.textContent = element.origin;

        /* CREAR BOTONES DINAMICAMENTE PARA REMOVER ATRIBUTOS */
        const newBtn = document.createElement("button");
        newBtn.textContent = "Remove href";
        newBtn.setAttribute("id", index);   // Identificar que boton es de que
        newBtn.setAttribute("class", "hrefRemove"); // Les coloco una clase para el evento
        $(this).parent().append(newBtn);
    });

    /* CLICK A BOTON CON CLASE hrefRemove */
    $(".hrefRemove").click(function() {
        removeHref(this);
    });

    /* AGREGAR LINKS NUEVOS */
    $("#myButtonAdd").click(function() {
        /* CREAR ELEMENTOS DOM */
        const newLi = document.createElement("li");
        const newA = document.createElement("a");

        /* Agregar atributos del link */
        newA.textContent = $("#myInputAdd").val();
        newA.setAttribute("href", $("#myInputAdd").val());
        newA.setAttribute("title", $("#myInputAdd").val());

        /* Agregar nodo <a> en <li> */
        newLi.appendChild(newA);
        /* Agregar nodo <li> en DOM */
        $("#myLinksList").append(newLi);

        /* AL AGREGAR BOTON LUEGO DEL EVENT HANDLER, TENGO QUE AGREGARLO ASÍ */
        // https://stackoverflow.com/questions/6205258/jquery-dynamically-create-button-and-attach-event-handler
        const newBtn = $("<button/>",
        {
            text: "Remove href",
            click: function() { removeHref(this); }
        });

        newBtn.attr("id", $("a").length - 1);

        /* CREAR BOTONES DINAMICAMENTE PARA REMOVER ATRIBUTOS */
        /*
        const newBtn = document.createElement("button");
        newBtn.textContent = "Remove href";
        newBtn.setAttribute("id", $("a").length);   // Identificar que boton es de que
        newBtn.setAttribute("class", "hrefRemove"); // Les coloco una clase para el evento
        */
        console.log(newBtn);
        $("a").last().parent().append(newBtn);
    });

});
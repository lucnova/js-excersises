/* -------------------------------------------------------------------------- */
/*                                    AJAX                                    */
/* -------------------------------------------------------------------------- */

$(document).ready(function () {
    'use strict';

    /* ---------------------------------- Load ---------------------------------- */
    /**
     * Obtiene datos e incrusta resultado en elemento del DOM seleccionado.
     * Incrusta HTML también.
     */

    //$("#myParagraph").load("https://reqres.in/");

    /* ----------------------------------- GET ---------------------------------- */

    $("#buttonGET").click(function () {
        $.getJSON("https://reqres.in/api/users", {
            page: 2
        }, function (response) {
            console.log("*** DATA GET");
            //console.log(response, typeof response);
            $("#myParagraph").text("");
            response.data.forEach(element => {
                let newP = document.createElement("p");
                newP.textContent = `Nombre: ${element.first_name} ${element.last_name}.
                Email: ${element.email}`;

                $("#myParagraph").append(newP);
                //console.log(element);
            });
        });
    });

    /* ---------------------------------- POST ---------------------------------- */

    $("#buttonPOST").click(function () {
        const customUserToPOST = {
            email: $("#usr_email").val(),
            password: $("#usr_pass").val()
        };

        /**
         * Recibe:
         *  - URL de API
         *  - el dato a enviar
         *  - Funcion que hace CallBack, devolviendo la respuesta
         *  - Y el tipo de dato que esperas que devuelva el servidor
         */

        $.post("https://reqres.in/api/login", customUserToPOST,
            (data, textStatus) => {
                console.log("*** SUCCESS");
                //console.log(data, typeof data);
                //console.log(textStatus, typeof textStatus);
                alert("POST correcto. Token: " + data.token);
            },
            "json"
        ).fail((response) => {
            console.log("*** ERROR");
            //console.log(response);
            alert(`ERROR ${response.status}: ${response.responseJSON.error}.`);
        });
    });
});
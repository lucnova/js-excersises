/* -------------------------------------------------------------------------- */
/*                              AJAX ALTERNATIVO                              */
/* -------------------------------------------------------------------------- */

/**
 *      Es otra sintaxis, creo que queda más limpia y específica.
 * 
 *  Timeout: Milisegundos de espera por la respuesta.
 */

$(document).ready(function () {
    'use strict';

    /* ----------------------------------- GET ---------------------------------- */

    $("#buttonGET").click(function () {
        $.ajax({
            type: "GET",
            url: "https://reqres.in/api/users",
            data: {
                page: 2
            },
            dataType: "json",
            beforeSend: () => {
                console.log("** Sending...");
            },
            success: (response) => {
                console.log("*** SUCCESS");
                $("#myParagraph").text("");
                response.data.forEach(element => {
                    let newP = document.createElement("p");
                    newP.textContent = `Nombre: ${element.first_name} ${element.last_name}.
                Email: ${element.email}`;

                    $("#myParagraph").append(newP);
                    //console.log(element);
                });
            },
            error: (response) => {
                console.log("*** ERROR");
                alert(`ERROR ${response.status}: ${response.responseJSON.error}.`);
            },
            timeout: 5000
        });
    });

    /* ---------------------------------- POST ---------------------------------- */

    $("#buttonPOST").click(function () {
        const customUserToPOST = {
            email: $("#usr_email").val(),
            password: $("#usr_pass").val()
        };

        $.ajax({
            type: "POST",
            url: "https://reqres.in/api/login",
            data: customUserToPOST,
            dataType: "json",
            beforeSend: () => {
                console.log("** Sending...");
            },
            success: (response) => {
                console.log("*** SUCCESS");
                alert("POST correcto.\nToken: " + response.token);
            },
            error: (response) => {
                console.log("*** ERROR");
                alert(`ERROR ${response.status}: ${response.responseJSON.error}.`);
            },
            timeout: 5000
        });
    });
});
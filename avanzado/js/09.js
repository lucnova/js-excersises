'use strict'
/* ------------- OBJETOS: JSON (JavaScript Object Notation) -------------- */
/**
 *  Define objetos tal cual, y puede ser ampliamente usado para esetructurar
 *      tipos de datos que se deseen en un mismo objeto.
 * 
 *  Se puede enviar y recibir datos mediante el uso de objetos JSON.
 * */

var myMovie = {
    title: "Pulp Fiction",
    director: "Quetin Tarantino",
    year: 1994,
    country_published: "United States"
};

var myMovieList = [
    myMovie,
    {
        title: "Shrek 2",
        director: "Dreamworks",
        year: 2004,
        country_published: "United States"
    },
    {
        title: "Sen to Chihiro no Kamikakushi",
        director: "Hayao Miyazaki",
        year: 2001,
        country_published: "Japan"
    }
];


for(myMovie of myMovieList) {
    let movieListingHTML = document.querySelector("#movieListing");
    let pElementHTML = document.createElement("p");
    let pTextHTML = document.createTextNode(`
        Title: ${myMovie.title} 
        - Year: ${myMovie.year}
        - Director: ${myMovie.director}
        - Country: ${myMovie.country_published}
    `);

    pElementHTML.appendChild(pTextHTML);
    movieListingHTML.appendChild(pElementHTML);
}



//console.log(myMovie);
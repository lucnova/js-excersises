'use strict'
/* ------------------ DOCUMENT OBJECT MODEL --------------------- */
/* SELECCIONAR A TODOS LOS ELEMENTOS DE UNA MISMA CLASE */
/* CON GETELEMENT */
console.log("* SELECCIONAR A TODOS LOS ELEMENTOS DE UNA MISMA CLASE *");
let myHeaderElements = document.getElementsByClassName("myHeaderClass");
console.log(myHeaderElements);

let maxElementsOnArray = myHeaderElements.length;
for(let i = 0; i < maxElementsOnArray; i++) {
    myHeaderElements[i].style.background = "red";
}

/* QUERY SELECTOR */
/* Mejor para navegadores modernos, pero getElements para soporte antiguo */
/* Y ademas deja seleccionar con lenguaje de CSS */
console.log("* SELECCIONAR A TODOS LOS ELEMENTOS DE UNA MISMA CLASE QUERYSELECTOR *");
myHeaderElements = document.querySelectorAll(".myHeaderClass");
console.log(myHeaderElements);
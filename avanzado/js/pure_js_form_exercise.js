'use strict'
/* ----------------- EJERCICIO INTEGRADOR -------------------- */
/* Crear un formulario con 3 campos; nombres apellidos y edad.
 *   Añadir botón con evento Submit.
 *   Mostrar los datos por pantalla.
 *   Tener un botón donde al hacer clic muestre los datos actuales del formulario.
 *       (no es submit).
 */

window.addEventListener('load', () => {
    /* Boton de mostrar datos */
    var myButtonShow = document.querySelector("#myButtonShow");
    /* Elemento form en el HTML */
    var myForm = document.querySelector("#myForm");
    /* Elemento section en el HTML para insertar nodos */
    var myShownData = document.querySelector("#myShownData");

    /* MOSTRAR DATOS DEL FORMULARIO EN EL HTML */
    function showDataInHTML() {
        /* Inputs del Form */
        let myInputs = document.querySelectorAll("input[type=text]");

        /* Creando elementos del DOM: Una Unsorted List*/
        let myULElement = document.createElement("ul");

        /* Para cada uno de los inputs del form */
        for (let myTextInput of myInputs) {
            // Creo el texto que tendrá el List Item (el texto del input)
            let myLIText = document.createTextNode(myTextInput.value);
            // Creo el elemento List Item
            let myLIElement = document.createElement("li");

            /* Agrego el texto al List Item */
            myLIElement.appendChild(myLIText);
            /* Agrego el List Item al Unsorted List*/
            myULElement.appendChild(myLIElement);
        }

        /* Si en la seccion ya hubo datos (si se hizo clic antes) */
        if (myShownData.children.length > 2) { // Si tiene más de dos hijos en la rama
            // Salto dos elementos porque existe el botón Show Data y el separador
            myShownData.removeChild(myShownData.lastElementChild); // Lo retiro
        }

        /* Agrego el nodo con datos obtenidos */
        myShownData.appendChild(myULElement);
    }


    /* AL HACER CLIC EN SUBMIT */
    myForm.addEventListener('submit', () => {
        console.log("* Enviando datos *");
        /* Obteniendo los valores de inputs y los labels */
        let myInputs = document.querySelectorAll("#myForm input[type=text]");
        let myInputsLabels = document.querySelectorAll("#myForm label");
        let myInputsLength = myInputs.length;

        /* Comprobar datos del form */
        for (let i = 0; i < myInputsLength; i++) {
            // El valor tal cual a comprobar
            let valueChecked = myInputs[i].value;
            // La label con el nombre recortado, extrayendo el texto sin el ":" y en minúsculas
            let myInputLabelName = myInputsLabels[i].textContent.trim().substr(0, myInputsLabels[i].textContent.length - 2).toLowerCase();

            /* Checar valores dependiendo de que tipo de dato sea */
            /* NOTA: Lo quize hacer dinámico, para evitar que cuando modifique el HTML, 
                        tener que rehacer más codigo */
            /*      Es buena idea comprobar en HTML también, pero esta es una prueba para comprobar en JS. */
            switch (myInputLabelName) {
                case "age": // Caso especial en que sea una edad
                    if (valueChecked.trim() === "" || valueChecked.trim().length == 0 || isNaN(parseInt(valueChecked))) {
                        console.log("* Dato no valido detectado *");
                        alert(`Please fill in valid data in: ${myInputsLabels[i].textContent.trim().replace(":", ".").toLowerCase()}`);

                        return false;
                    }
                    break;

                default:    // Caso por defecto, donde son textos
                    if (valueChecked.trim() === "" || valueChecked.trim().length == 0) {
                        console.log("* Dato no valido detectado *");
                        alert(`Please fill in valid data in: ${myInputsLabels[i].textContent.trim().replace(":", ".").toLowerCase()}`);

                        return false;
                    }
                    break;
            }
        }

        /* Para cada uno de los inputs del form, "enviarlos" */
        for (let i = 0; i < myInputsLength; i++) {
            console.log(myInputsLabels[i].textContent.trim(), myInputs[i].value);
        }

        showDataInHTML();
    });


    /* AL PRESIONAR EL BOTON DE MOSTRAR DATOS DEL FORMULARIO */
    myButtonShow.addEventListener('click', () => {
        showDataInHTML();
    });
})
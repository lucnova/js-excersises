'use strict';
/* -------------------------------------------------------------------------- */
/*                               MANEJAR ERRORES                              */
/* -------------------------------------------------------------------------- */
let myURL = "https://developer.mozilla.org/";

// Esto causa error
//let myURL = "ht%&%$#tps://developer.mozilla.org/";

// Estructura Try-Catch
try {
    // Todo el código que sea suceptible a errores entra en el TRY.
    console.log(decodeURIComponent(myURL));
} catch (error) {
    // Si llega a saltar error, entonces lo atrapas acá.
    // Justo como en C++ o C#.
    console.log(error);
}

console.log("Without that try-catch I wouldn't be displayed.");
console.log("All the code executed correctly.");
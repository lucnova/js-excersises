'use strict'
/* ------------- OBJETOS: THIS -------------- */
/**
 *  This es un operador que se usa para referirte al objeto en sí.
 *  Si quieres referirte al objeto mismo en una funcion o método,
 *      se usa this.
 * 
 *  (Justo como se usaria en objetos como C++).
 * */
var myHeaderTitle = document.querySelector("#myHeaderTitle");
var myButtonClick = document.querySelector("#myButtonClick");
var colorIsToggled = false;

function toggleButtonColor() {
    //console.log(colorIsToggled);
    if (colorIsToggled) {
        myHeaderTitle.style.background = "red";
        colorIsToggled = false;
    } else {
        myHeaderTitle.style.background = "green";
        colorIsToggled = true;
    }
}

/*
 * Event Listener
 */

/**
 *  NOTA: "this" no funciona en funciones flechas, solo funciona usar this
 *      cuando usas "function(){}".
 *  */

myButtonClick.addEventListener('click', function() {
    toggleButtonColor();
    //console.log(this);
    this.style.background = "red";
});
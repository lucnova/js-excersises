'use strict'
/*
    Que veré en el curso:
    DOM:    Document Object Model   -> Manipular el documento (página web)
    BOM:    Browser Object Model    -> Manipular navegador y ventana
    Eventos:    Cosas que suceden en las interacciones entre usuario y página.
    JSON:   Notacion de objetos de javascript; hacer objetos.
    LocalStorage:   Almacenamiento local del navegador, para persistir en la sesion
    Peticiones Asíncronas:  Fetch, AJAX, Promesas...
    Fechas y Errores. Etc.
*/
/* ------------------ DOCUMENT OBJECT MODEL --------------------- */
// Modificar cosas como tags del cuerpo del html.
// El DOM es como se estructura una página HTML en base a nodos.
// **NOTA**: Si se quiere manipular DOM, cargar script al final del documento.
//          NO cargarlo en head.

/* Obteniendo el elemento por ID */
console.log("* Obteniendo el elemento por ID *");
let myDivBox = document.getElementById("myDivBox");
console.log(myDivBox);

/* Obteniendo el elemento por ID y sacando su InnerHTML */
console.log("* Obteniendo el elemento por ID y sacando su InnerHTML *");
myDivBox = document.getElementById("myDivBox").innerHTML;
console.log(myDivBox);

/* QUERY SELECTOR */
myDivBox = document.querySelector("#myDivBox"); // Seleccionar por ID
//myDivBox = document.querySelector(".myClass"); // Seleccionar por clase

/* Manipulando objeto */
myDivBox = document.getElementById("myDivBox");
myDivBox.innerHTML = "** This is a change made in JS.";
// Se puede manipular el estilo CSS
myDivBox.style.backgroundColor = "red";
myDivBox.style.padding = "20px";
myDivBox.style.color = "white";
// Modificar la clase
myDivBox.className = "myClass";

'use strict';

/* -------------------------------------------------------------------------- */
/*                            FECHAS Y MATEMÁTICAS                            */
/* -------------------------------------------------------------------------- */

/* --------------------------------- Fechas --------------------------------- */

let myDate = new Date();
let myFullData = 
`El dia de hoy es: ${myDate.getDay()} del mes ${myDate.getMonth()} del año ${myDate.getFullYear()}.
Hora Actual: ${myDate.getHours()}:${myDate.getMinutes()}:${myDate.getSeconds()}
`;

console.log(myFullData);

/* ------------------------------- Matemáticas ------------------------------ */

console.log("Number 0-100:", Math.floor(Math.random() * 100));
console.log("Number 0-10:", Math.floor(Math.random() * 10));
console.log("PI:", Math.PI);
let x = Math.floor(Math.random() * 100);
console.log("Sin(X):", Math.sin(x));
console.log("where x = ", x);
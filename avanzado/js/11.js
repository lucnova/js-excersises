'use strict';
/* -------------------------------------------------------------------------- */
/*                        Fetch y Peticiones Asíncronas                       */
/* -------------------------------------------------------------------------- */

/**
 *  Es una llamada a un servicio API REST/Backend.
 *      Permite hacer peticiones AJAX, que devuelve un resultado
 *  y se hace lo que se desee con ese resultado.
 * 
 *      Son peticiones asíncronas.
 * 
 *      Clásicamente se hacen con la libreria JQuery.
 *      Pero con los estándares de JavaScript nuevos se puede usar esta
 *      funcion.
 *  
 */

/* FETCH: Primer parametro es la URL al servicio 
 *   Fetch devuelve una promesa, a la cual puedes llamarle un método "then()"
 *       esta recoge datos, es una funcion callback la que devuelve
 *       
 */
function getUsers() {
    //fetch("https://jsonplaceholder.typicode.com/users") // Llamada a la URL especificada.
    return fetch("https://reqres.in/api/users?page=2");
}

function getSingleUser() {
    return fetch("https://reqres.in/api/users/2");
}

function getCustomPromise() {
    let myJSONObj = {
        name: "Hi",
        last_name: "Im a JSON Object",
        url: "https://developer.mozilla.org/"
    };

    /* Declarar un objeto Promise: Recibe una funcion callback */
    /*      Devuelve resolve y reject como parametros */
    return new Promise((resolve, reject) => {
        let myNewJSONStr = "";

        setTimeout(() => { // Simular lag de internet
            myNewJSONStr = JSON.stringify(myJSONObj);
            if (typeof myNewJSONStr != "string" || myNewJSONStr == "") return reject("ERROR.");

            return resolve(myNewJSONStr);
        }, 3000);
    });
}

function listUserInDOM(currentUser) {
    let usersListHTML = document.querySelector("#singleUser");
    // Remover el mensaje de carga.
    usersListHTML.removeChild(usersListHTML.lastElementChild);

    // Crear elementos del DOM HTML
    let h2HTMLElement = document.createElement("h3");
    let imgHTMLElement = document.createElement("img");
    let h2HTMLText = document.createTextNode(`
            ${currentUser.first_name}
        `);

    // Asignar elemento avatar al elemento img
    imgHTMLElement.src = currentUser.avatar;
    imgHTMLElement.width = '128';

    // Agregar los nodos
    h2HTMLElement.appendChild(h2HTMLText);
    usersListHTML.appendChild(h2HTMLElement);
    usersListHTML.appendChild(imgHTMLElement);
}

function listUsersInDOM(usersList) {
    let usersListHTML = document.querySelector("#usersList");
    // Remover el mensaje de carga.
    usersListHTML.removeChild(usersListHTML.lastElementChild);

    usersList.forEach((currentUser, i) => {
        // Crear elementos del DOM HTML
        let h2HTMLElement = document.createElement("h3");
        let imgHTMLElement = document.createElement("img");
        let h2HTMLText = document.createTextNode(`
                ${i} .- ${currentUser.first_name}
            `);

        // Asignar elemento avatar al elemento img
        imgHTMLElement.src = currentUser.avatar;
        imgHTMLElement.width = '128';

        // Agregar los nodos
        h2HTMLElement.appendChild(h2HTMLText);
        usersListHTML.appendChild(h2HTMLElement);
        usersListHTML.appendChild(imgHTMLElement);
    });
}

/**
 * FETCH CONCATENADO:   Para obtener datos luego de haber obtenido otros.
 *          y evitar asincronia y errores.
 *      Estructura:
 *          - Fetch
 *          - Data to JSON Conversion
 *          - Data Manipulation
 */

console.log("*** GATHERING USERS ***");
getUsers()
    .then(data => data.json()) // La respuesta se captura y se
    //   puede converitr a JSON.
    // NOTA: Al usar funcion con "{}"
    //      poner respetivo return.
    .then(data => { // Luego de ser convertida a JSON es usable.
        console.log("*** DONE ***");
        /* Se maneja los datos a deseo */
        listUsersInDOM(data.data);

        console.log("*** GATHERING SINGLE USER ***");
        return getSingleUser(); // Fetch, retorna una promesa.
    })
    .then(data => data.json())
    .then(data => {
        console.log("*** DONE ***");
        listUserInDOM(data.data);

        console.log("*** GATHERING CUSTOM JSON ***");
        return getCustomPromise();
    })
    .then(data => {
        console.log("*** DONE ***");
        console.log(data);
    })
    .catch(error => {   // EN CASO DE ERROR ATRAPARLO
        console.log("*** ERROR: ", error);
    }) 
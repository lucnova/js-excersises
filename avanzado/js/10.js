'use strict'
/* ------------- LocalStorage -------------- */
/**
 *  También llamado HTML5 WebStorage, se usa para guardar info en el 
 *      navegador a modo de sesion y esté disponible en toda la navegación
 *      de la página web.
 *  Se usa muchisimo en frameworks de apps en JS.
 * 
 *  LocalStorage podria no estar disponible en navegadores antiguos.
 * */

// * Comprobar si LocalStorage esta disponible. *
if(typeof(Storage) !== 'undefined') {
    console.log("LocalStorage Disponible.");
}
else {
    console.log("LocalStorage no está disponible.");
}

// * Creando un objeto película *
var myMovie = {
    title: "Pulp Fiction",
    director: "Quetin Tarantino",
    year: 1994,
    country_published: "United States"
};

// * Guardando en localstorage. *
localStorage.setItem('someVar', 'Hello!');
//  (convirtiendolo a string pues no deja guardarlo como JSON) *
localStorage.setItem('aMovie', JSON.stringify(myMovie));

// * Obteniendo datos de local storage *
console.log(localStorage.getItem('someVar'));

// * Obteniendo un JSON en forma string *
myMovie = JSON.parse(localStorage.getItem('aMovie'));
console.log(JSON.parse(localStorage.getItem('aMovie')));

// * Listando en HTML *
let movieListingHTML = document.querySelector("#movieListing");
let pElementHTML = document.createElement("p");
let pTextHTML = document.createTextNode(`
    Title: ${myMovie.title} 
    - Year: ${myMovie.year}
    - Director: ${myMovie.director}
    - Country: ${myMovie.country_published}
`);

pElementHTML.appendChild(pTextHTML);
movieListingHTML.appendChild(pElementHTML);

// * Limpieza de LocalStorage *
//      Para remover uno en especifico es .remove()
localStorage.clear();
'use strict'
/* ------------------ BROWSER OBJECT MODEL --------------------- */
// Redireccionamiento, tamaño de ventana, etc.
var windowObjectReference = null; // global variable

// Mostrar las propiedades de la URL actual en una ventana emergente:
function showLoc() {
    var oLocation = location,
        aLog = ["Property (Typeof): Value", "location (" + (typeof oLocation) + "): " + oLocation];
    for (var sProp in oLocation) {
        aLog.push(sProp + " (" + (typeof oLocation[sProp]) + "): " + (oLocation[sProp] || "n/a"));
    }
    alert(aLog.join("\n"));
}

//Enviar una cadena de caracteres de datos al servidor por modificar la propiedad search:
// Como si fueran parametros get
function sendData(sData) {
    location.search = sData;
}

/* Mejor práctica para no abrir pestañas nuevas cada vez, si no reusar la que estaba ya abierta. */
function openFFPromotionPopup() {
    if (windowObjectReference == null || windowObjectReference.closed)
    /* if the pointer to the window object in memory does not exist
       or if such pointer exists but the window was closed */

    {
        windowObjectReference = window.open("http://www.spreadfirefox.com/",
            "PromoteFirefoxWindowName", "resizable,scrollbars,status");
        /* then create it. The new window will be created and
           will be brought on top of any other window. */
    } else {
        windowObjectReference.focus();
        /* else the window reference must exist and the window
           is not closed; therefore, we can bring it back on top of any other
           window with the focus() method. There would be no need to re-create
           the window or to reload the referenced resource. */
    };
}

/**
 * En código, usar:
 * <p><a
    href="http://www.spreadfirefox.com/"
    target="PromoteFirefoxWindowName"
    onclick="openFFPromotionPopup(); return false;" 
    title="This link will create a new window or will re-use an already opened one"
    >Promote Firefox adoption</a></p>

    Esto porque al usar el onclick con return false; 
    se ejecuta el javascript y no la accion default de HTML,
    pero si el navegador esta sin JavaScript, entonces solo hace lo mismo pero
    con HTML.
    Y como el target tiene nombre, si ya se abrio con un target: "PromoteFirefoxWindowName"
    entonces la crea y abre.
 */



// Obtener la anchura interna de la pantalla del navegador
console.log("* Obteniendo el ancho de la ventana *");
console.log(window.innerWidth);
console.log(screen.width);

// Obtener la altura interna de la pantalla del navegador
console.log("* Obteniendo la altura de la ventana *");
console.log(window.innerHeight);
console.log(screen.height);

/* Con el screen puedo obtener calculos ya hechos por el navegador */

// Obtener datos de la ubicacion actual del documento
//  "Esto significa que puedes trabajar con location como 
//  si fuera una cadena de caracteres en la mayoría de los casos: 
//  location = 'http://www.example.com' es un sinónimo de 
//  location.href = 'http://www.example.com'.
console.log(window.location);

// Redireccionar página
//location.replace("http://www.mozilla.org");   // No deja un historial
//location.assign("http://www.mozilla.org");    // Se registra en historial
//location.href = "http://www.mozilla.org";

// Abrir una pestaña nueva con una url
//  No recomendable pues existen bloqueadres de popups
//window.open("http://www.mozilla.org");
//let myWindowFeatures = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";
let myWindowFeatures = "width=512,height=512,menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";
let myPopUpWindow = window.open("http://www.mozilla.org", "MDN_Window", myWindowFeatures);
console.log(myPopUpWindow);

// Open devuelve un objeto usado de wrapper de ventana, 
//      Ahora comprendo como ventanas con contadores de tiempo funcionan.




// Historial del navegador
console.log(history.length);
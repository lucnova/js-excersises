'use strict'
/* ------------- Ejercicio LocalStorage -------------- */
var addMovieForm = document.querySelector("#addMovieForm");
var rmvMovieForm = document.querySelector("#rmvMovieForm");

/* Listar LocalStorage en DOM */
function listLocalStorageOnDOM() {
    let movieListing = document.querySelector("#movieListing");
    movieListing.textContent = '';

    Object.values(localStorage).forEach((localItem) => {
        let pTextContent = document.createTextNode(localItem);
        let pElement = document.createElement("p");

        pElement.appendChild(pTextContent);
        movieListing.appendChild(pElement);
    });
}

addMovieForm.addEventListener('submit', function() {
    /* Variables del DOM */
    let movieNameInput = document.querySelector("#movieNameInput");

    //console.log(movieNameInput.value);

    /* Comprobar que no sea vacío */
    let movieName = movieNameInput.value;
    if(movieName.length >= 1) { // Guardo en caso que no sea vacío
        localStorage.setItem(movieName, movieName);
        
        // Muestro solo ese elemento en DOM
        listLocalStorageOnDOM();
    }
});

rmvMovieForm.addEventListener('submit', function() {
    /* Variables del DOM */
    let movieNameInputRmv = document.querySelector("#movieNameInputRmv");

    //console.log(movieNameInput.value);

    /* Comprobar que no sea vacío */
    let movieName = movieNameInputRmv.value;
    if(movieName.length >= 1) { // Guardo en caso que no sea vacío
        localStorage.removeItem(movieName);
    }
    listLocalStorageOnDOM();
});

// Mostrar todos los elementos de LocalStorage en DOM
listLocalStorageOnDOM();
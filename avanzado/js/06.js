'use strict'
/* ------------- EVENTOS DE FOCO, DE TECLADO Y EVENTO LOAD -------------- */
/** Son eventos que use ya en C# usando Visual Studio */

// EVENTO LOAD: Este evento ejecuta la funcion cuando esta cargado el elemento.
//      En este caso, la ventana del navegador cargando el documento.
//      De esta forma puedo cargar el script al inicio de la página y no al final.
//      Aunque: "This function basically says, don't run any of the code 
//              inside until the document is ready, or fully loaded. 
//              This will prevent any errors, but it can still slow down the 
//              loading time of your HTML, which is why it is still best to 
//              include the script after all of the HTML."
window.addEventListener('load', () => {
    var myTextInputA = document.querySelector("#myTextInputA");
    var myTextInputB = document.querySelector("#myTextInputB");
    var myTextInputC = document.querySelector("#myTextInputC");
    var myTextInputD = document.querySelector("#myTextInputD");
    var myTextInputE = document.querySelector("#myTextInputE");

    var myHeaderTitle = document.querySelector("#myHeaderTitle");
    var colorIsToggled = false;

    function toggleButtonColor() {
        //console.log(colorIsToggled);
        if (colorIsToggled) {
            myHeaderTitle.style.background = "red";
            colorIsToggled = false;
        } else {
            myHeaderTitle.style.background = "green";
            colorIsToggled = true;
        }
    }

    // EVENTO FOCUS: Cuando se entra al foco del elemento.
    myTextInputA.addEventListener('focus', () => toggleButtonColor());

    // EVENTO BLUR
    myTextInputB.addEventListener('blur', () => toggleButtonColor());

    // EVENTO KEYDOWN: En cuanto se presiona la tecla se registra el evento.
    //      Tambien registra teclas como Shift y Enters.
    //      Captura SOLO mayúsculas, aun que sean minusculas las teclas.
    //  se puede obtener datos del evento en el callback agregando una variable
    //  event.keyCode en si devuelve el valor numérico de la tecla presionada.
    //  Como en C, C++ o C# que los caracteres son ASCII numerico, aquí son Unicode.
    //  fromCharCode los convierte la secuencia numerica en cadena.
    myTextInputC.addEventListener('keydown', (event) => {
        console.log("[KeyDown]", String.fromCharCode(event.keyCode));
        toggleButtonColor();
    });

    // EVENTO KEYPRESS: Al presionar una tecla, no importa si se suelta luego.
    //                  No registra teclas como Shift ni Enter.
    //                  Captura mayus/minus
    myTextInputD.addEventListener('keypress', (event) => {
        console.log("[KeyPress]", String.fromCharCode(event.keyCode));
        toggleButtonColor();
    });

    // EVENTO KEYUP: Al soltar la tecla presionada.
    //      Tambien registra teclas como Shift y Enters.
    //      Captura SOLO mayúsculas, aun que sean minusculas las teclas.
    myTextInputE.addEventListener('keyup', (event) => {
        console.log("[KeyUp]", String.fromCharCode(event.keyCode));
        toggleButtonColor();
    });
})
'use strict'
/* ------------------ DOCUMENT OBJECT MODEL --------------------- */
/* SELECCIONAR A TODOS LOS ELEMENTOS DE UNA MISMA ETIQUETA */
console.log("* SELECCIONAR A TODOS LOS ELEMENTOS DE UNA MISMA ETIQUETA *");
let myDivs = document.getElementsByTagName("div");
console.log(myDivs);

// Se guardan todos los elementos en un arreglo
console.log("* ACCEDIENDO POR INDICE *");
let myDiv = myDivs[2];
console.log(myDiv.textContent.trim());

/* GUARDANDO TEXTOS A ARREGLO ESTATICO */
let myDivsStrArray = Array();
for(let i = 0; i < myDivs.length; i++) {
    myDivsStrArray.push(myDivs[i].textContent.trim());
}

/* CAMBIANDO TERCER DIV */
console.log("* CAMBIANDO PROPIEDADES *");
myDiv.innerHTML = "**Huzzah... This was modified in JS.";
myDiv.style.backgroundColor = "red";
myDiv.style.padding = "20px";
myDiv.style.color = "white";

/* CREANDO NUEVOS NODOS */
console.log("* CREANDO NUEVOS NODOS *");

/* NOTA: No se puede usar apropiadamente foreach, o for of sin antes
        no convertirlos a arreglos.
        En este caso usare un for simple.
        A la hora de usar getElementsByTagName devuelve una HTMLCollection
        que es dinamica, si modificas en codigo tambien se modifica (No es estatica)
 */
// CONVERTIR A ARREGLO ESTATICO
//let myDivsArray = Array.prototype.slice.call(myDivs); // Esto es mas ampliamente compatible
//let myDivsArray = [...myDivs];  // Esto es mas eficiente en runtime: https://jsben.ch/h2IFA
                                // ECMAScript 2015

for(let myDivStr of myDivsStrArray) {
    let myParagraph = document.createElement("div");
    let myText = document.createTextNode(myDivStr);
    console.log(myDivStr);

    myParagraph.appendChild(myText);
    document.querySelector("#myJsSection").appendChild(myParagraph);
    // append tambien sirve pero es experimental.
    // Tambien puedo usar prepend en caso de querer ponerlo antes del elemento
}
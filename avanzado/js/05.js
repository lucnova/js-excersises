'use strict'
/* ------------------ EVENTOS --------------------- */
/* Funciones que se ejecutan cuando son disparadas porque suceden */
var myHeaderTitle = document.querySelector("#myHeaderTitle");
var myButtonClick = document.querySelector("#myButtonClick");
var myButtonDblClick = document.querySelector("#myButtonDblClick");
var myButtonMouseOver = document.querySelector("#myButtonMouseOver");
var myButtonMouseOut = document.querySelector("#myButtonMouseOut");
var colorIsToggled = false;

function toggleButtonColor() {
    console.log(colorIsToggled);
    if (colorIsToggled) {
        myHeaderTitle.style.background = "red";
        colorIsToggled = false;
    } else {
        myHeaderTitle.style.background = "green";
        colorIsToggled = true;
    }
}


/*
 * Event Listener
 */

/* Click */
// Recibe nombre del evento y funcion a ejecutar en forma de callback
myButtonClick.addEventListener('click', () => toggleButtonColor());

/* Doble Click */
myButtonDblClick.addEventListener('dblclick', () => toggleButtonColor());

/* Mouse Over */
myButtonMouseOver.addEventListener('mouseover', () => toggleButtonColor());

/* Mouse Out */
myButtonMouseOut.addEventListener('mouseout', () => toggleButtonColor());
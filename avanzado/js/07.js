'use strict'
/* ------------- TIMERS -------------- */
window.addEventListener('load', () => {
    var myHeaderTitle = document.querySelector("#myHeaderTitle");
    var myButtonBoo = document.querySelector("#myButtonBoo");
    var myButtonStop = document.querySelector("#myButtonStop");

    var colorIsToggled = false;

    function toggleButtonColor() {
        //console.log(colorIsToggled);
        if (colorIsToggled) {
            myHeaderTitle.style.background = "red";
            colorIsToggled = false;
        } else {
            myHeaderTitle.style.background = "green";
            colorIsToggled = true;
        }
    }

    /* ----------- Timers ---------- */
    // setInterval: Ejecuta una funcion anónima cada cierto tiempo.
    // setInterval recibe dos parametros, una funcion callback y otro el tiempo en el que se ejcutara
    //      1000 -> 1 seg
    var myTimeInterval = setInterval(() => {
        console.log("It's been 4 seconds, time for your color change :)");
        console.log("Okay");
        toggleButtonColor();
    }, 4000);

    // setTimeout: Ejecuta una funcion anónima en un cierto tiempo pero solo 1 vez.
    myButtonBoo.addEventListener('click', () => {
        let myTimeOut = setTimeout(() => {
            let myButtonSection = document.querySelector("#myButtonSection");
            let myH1Element = document.createElement("h1");
            let myH1Text = document.createTextNode("Boo!");

            myH1Element.appendChild(myH1Text);

            myButtonSection.appendChild(myH1Element);

            console.log("Gotcha!");
        }, 3000)
    });

    // clearInterval limpia el intervalo.
    myButtonStop.addEventListener('click', () => {
        console.log("Okay, I'll stop.");
        clearInterval(myTimeInterval);
    });

})
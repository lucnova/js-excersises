'use strict'
/* -------------- PLANTILLAS DE TEXTO --------------- */
// EcmaScript 6 nuevo
let myName = prompt("¿Cual es tu nombre?", "Miguel");
let myLastName = prompt("¿Cual es tu apellido paterno?", "Locomotor");

/*
    OJO: No es comilla simple de cadena ni acento agudo ('), 
        es comilla simple invertida (Nombre correcto: Acento grave): `
*/
let myStr = `
    <h1>Me presento</h1>
    <h3>Mi nombre es ${myName}</h3>
    <h3>Mi apellido es ${myLastName}</h3>
`;
// Al usar ${variable} se le llama interpolar variables

document.write(myStr);
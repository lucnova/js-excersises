'use strict'
/* -------------- BUSQUEDAS-------------- */
let myStringA = "Texto de prueba de JavaScript";

// Buscar el índice desde donde empieza a existir la cadena buscada (SOLO PRIMER COINICIDENCIA)
// Retorna -1 si no existe.
let mySearch = myStringA.indexOf("de");
//search es alias de: indexOf
console.log(mySearch, typeof mySearch);

// Buscar el índice desde donde termina de existir la cadena buscada (SOLO ULTIMA COINCIDENCIA)
mySearch = myStringA.lastIndexOf("de");
console.log(mySearch, typeof mySearch);

// Buscar varias coincidencias
mySearch = myStringA.match(/de/g);   // Expresion regular /de/g     /texto_a_buscar/  y g de global
console.log(mySearch, typeof mySearch);

// Extraer segmentos de cadenas
mySearch = myStringA.substr(6, 9);  // Desde indice N retira M letras
console.log(mySearch, typeof mySearch);

// Seleccionar una letra en concreto
mySearch = myStringA.charAt(6);
console.log(mySearch, typeof mySearch);

// Saber si se encuentra o no una palabra DESDE EL INICIO de la cadena indicada
mySearch = myStringA.startsWith("Texto");
console.log(mySearch, typeof mySearch);

// Saber si se encuentra o no una palabra DESDE EL FINAL de la cadena indicada
mySearch = myStringA.endsWith("JavaScript");
console.log(mySearch, typeof mySearch);

// Saber si se encuentra o no una palabra en CUALQUIER LADO
mySearch = myStringA.includes("JavaScript");
console.log(mySearch, typeof mySearch);
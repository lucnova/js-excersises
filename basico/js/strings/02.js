'use strict'
/* -------------- REEMPLAZOS-------------- */
let myStringA = "Texto de prueba de JavaScript";

// Reemplazar un texto por otro
let myNewString = myStringA.replace("JavaScript", "HTML");
console.log(myNewString);

// Dividir cadena desde el indice indicado
myNewString = myStringA.slice(9);   // Puedes hacer que se comporte como substr solamente que indicando indices
console.log(myNewString);

// Separar cadena usando una cadena indicada como separadora
myNewString = myStringA.split(" ");
console.log(myNewString);
myNewString = myStringA.split("de");
console.log(myNewString);

// Remover espacios iniciales y finales de la cadena ("Limpiar" cadena de espacios en blanco)
let myStringB = "       Texto de prueba de JavaScript     2    ";
console.log(myStringB);
myNewString = myStringB.trim();
console.log(myNewString);
'use strict'
/* --------------------------- ARREGLOS PARTE 2 ----------------------- */
let myArray = ["McDonalds", "Taco Bell", "Pizza Hut", "Five Guys"];

// CONCATENAR elemento al FINAL del array
console.log("** Agregando elemento **");
myArray.push("Burger King");
console.log(myArray);

// ORDENAR todos los elementos del arreglo
console.log("** Ordenando elementos **");
myArray.sort();
console.log(myArray);

// INVERTIR POSICION de todos los elementos del arreglo
console.log("** Invirtiendo elementos **");
myArray.reverse();
console.log(myArray);

// RETIRAR elemento del FINAL del array
console.log("** Removiendo ultimo elemento **");
myArray.pop();
console.log(myArray);

// RETIRAR elemento CONCRETO del array
let myIndexToRemove = myArray.indexOf("Five Guys");
console.log("** Removiendo elemento especifico **");
if(myIndexToRemove != -1) {
    myArray.splice(myIndexToRemove, 1);
    console.log(myArray);
}
else {
    console.log("El indice no se retiró pues no se encontró.");
}

// CONCATENAR CADA ELEMENTO del array entre si DADA UNA STRING
console.log("** Uniendo elementos haciendo una string **");
let myStr = myArray.join(" y ");
console.log(myStr);

// SEPARAR ELEMENTOS de una string en un array DADA UNA STRING
console.log("** Separando string haciendo un arreglo **");
let myNewArray = myStr.split(" y ");
console.log(myNewArray);
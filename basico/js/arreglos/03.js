'use strict'
/* --------------------------- ARREGLOS: FOR IN ----------------------- */
// RECORRER ARRAY:
let myArray = ["McDonalds", "Taco Bell", "Pizza Hut", "Five Guys"];
myArray.foo = "hola";

document.write("<h1>Quiero comer en:</h1>");
document.write("<ul>");

//  ForIn: En arreglos funciona como un foreach pero ojo con usar con objetos;
//      Itera sobre las propiedades de el objeto
for(let myArrayElement in myArray) {
    document.write("<li>" + myArrayElement + "</li>");
}
document.write("</ul>");

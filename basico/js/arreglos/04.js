'use strict'
/* --------------------------- BUSQUEDAS ----------------------- */
let myArray = ["McDonalds", "Taco Bell", "Pizza Hut", "Five Guys", "Burger King"];
let myNumArray = [16, 32, 64, 128, 256];

/* ENCONTRAR UN ELEMENTO Y OBTENER ESE MISMO ELEMENTO */
//  let mySearch = myArray.find(myElement => {  return myElement == "Pizza Hut";    });
// Si solo se hace un return, se puede poner asi:
let mySearch = myArray.find(myElement => myElement == "Pizza Hut");
console.log(mySearch);

/* ENCONTRAR UN ELEMENTO Y OBTENER SU INDICE EN EL ARREGLO */
mySearch = myArray.findIndex(myElement => myElement == "Pizza Hut");
console.log(mySearch);

/* SABER SI ALMENOS UN ELEMENTO de un ARREGLO CUMPLE UNA CONDICION */
mySearch = myNumArray.some(myElement => myElement >= 60);
console.log(mySearch);
'use strict'
/* --------------------------- ARREGLOS ----------------------- */
// En JS los arreglos pueden guardar literlamente cualquier tipo de variable
let myArray = ["Hola", 6080, 12.1, true];
console.log(myArray);

// Array dentro de arrays
myArray = [["Hola", typeof "Hola"], [6080, typeof 680], [12.1, typeof 12.1], [true, typeof true]];
console.log(myArray);

// Otra manera de declararlos:
let myNewArray = new Array("Hola", 6080, 12.1, true);
console.log(myNewArray);

// Acceder a los indices de un array
console.log(myNewArray[0]); // Un elemento para UNIdimensional
console.log(myArray[0][1]); // Un elemento para MULTIdimension

// RECORRER ARRAY:
myArray = ["McDonalds", "Taco Bell", "Pizza Hut", "Five Guys"];

document.write("<h1>Quiero comer en:</h1>");
document.write("<ul>");
// For tradicional
/*
for(let i = 0; i < myArray.length; i++) {
    document.write("<li>" + myArray[i] + "</li>");
}
*/
//  ForEach: Hace una funcion sobre cada elemento; la funcion se 
//      recibe hace callback a ella, tu la defines
//  ** Lei que es obsoleto desde ECMAScript347, en su lugar es mejor for of
//  *** En realidad es obsoleta la funcion general, pero no el metodo del arreglo
//      Usar: for of()/for in()         NO usar: for each()
//             arr.foreach()
/*
myArray.forEach((myElement, myIndex, myData) => { // Tres parametros, puede ser opcional los otros dos
    // Elemento actual, Indice actual, Arreglo Original
    console.log(myData);
    document.write("<li>" + myIndex + ".- " + myElement + "</li>");
});
*/
//  ForOf:  Itera exactamente igual que foreach, solamente 
for (const myElement of myArray) {
    document.write("<li>" + myIndex + ".- " + myElement + "</li>");
}
document.write("</ul>");

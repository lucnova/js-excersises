'use strict'
/* ------------------- EJERCICIO 01 ------------------- */
/* Programa que pida dos números y que diga cual es mayor, 
cual el menor y si son iguales */

let myValA = parseInt(prompt("Valor A:", 0));
let myValB = parseInt(prompt("Valor B:", 0));

while(isNaN(myValA) || isNaN(myValB) || myValA < 0 || myValB < 0) {
    myValA = parseInt(prompt("Valor A:", 0));
    myValB = parseInt(prompt("Valor B:", 0));
}

if(typeof myValA == "number" && typeof myValB == "number" && myValA >= 0 && myValB >= 0) {
    if(myValA > myValB) {
        console.log(myValA + " es mayor que " + myValB);
        console.log(myValB + " es menor que " + myValA);
    }
    else if(myValA < myValB) {
        console.log(myValB + " es mayor que " + myValA);
        console.log(myValA + " es menor que " + myValB);
    }
    else if(myValA == myValB) {
        console.log(myValA + " es igual que " + myValB);
    }
    else {
        console.log("ERROR: Caso no previsto.");
    }
}
else {
    console.log("Por favor, introduce solo números enteros positivos.");
}

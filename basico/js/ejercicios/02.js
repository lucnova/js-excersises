'use strict'
/* --------------- EJERCICIO SOBRE ARREGLOS -------------- */
/*
    Hacer un programa que:
    1)  Pida 6 numeros y meterlos en un array.
    2)  Mostrar todos los elementos del array en el cuerpo de la pagina y consola.
    3)  Ordenar el array y mostrarlo.
    4)  Invertir el orden del array y mostrarlo.
    5)  Mostrar cuantos elementos tiene el array.
    6)  Hacer una búsqueda de un valor que el usuario indique, y mostrar su indice.
*/
/* Definicion de variables */
const MAX_VALUES = 6;   // Cantidad maxima de valores a introducir por el usuario
let myValues = new Array(MAX_VALUES);   // El arreglo donde se meterán los valores
let myInputVal, i;  // Valor donde guardare el valor que introduzcan y contador

/* 1)   PEDIR VALORES */
i = 0;
do {
    // Pregunto por el valor y lo guardo en una variable
    myInputVal = parseInt(prompt(`Introduce el valor #${i + 1}`));
    //  **NOTA** Recibi el error: NS_ERROR_XPC_SECURITY_MANAGER_VETO, no tengo idea de que signifique
    //      pero imagino tiene que ver con el promt mostrado y el tiempo de espera?

    // Si el valor no es NaN (No es una tonteria) agrego al indice del arreglo y cuento siguiente.
    if(!isNaN(myInputVal)) {
        myValues[i] = myInputVal;
        i++;
    }
}while(i < MAX_VALUES); // Hasta que los valores se acaben

/* 2)   MOSTRAR LOS VALORES */
document.write("<h1>Estos son los valores: </h1>");
document.write("<ul>");
for (const myValue of myValues) {
    document.write(`<li>${myValue}</li>`);
    console.log(myValue);
}
document.write("</ul>");

/* 3)   MOSTRAR LOS VALORES ORDENADOS */
console.log("**ORDENANDO ELEMENTOS**");
myValues.sort((a, b) => a - b); // Ordenar en orden numérico, si es negativo, a es menor que b
                                //, si es positivo, a es mayor que b, y si es 0, son iguales
/**
 *  Para entender mejor: 
 *  if (a < b) {
        return -1;
    }
    if (a > b) {
        return 1;
    }
    return 0;
 */
document.write("<h1>Estos son los valores en ORDEN: </h1>");
document.write("<ul>");
for (const myValue of myValues) {
    document.write(`<li>${myValue}</li>`);
    console.log(myValue);
}
document.write("</ul>");

/* 4)   MOSTRAR LOS VALORES INVERTIDOS */
console.log("**INVIRTIENDO ELEMENTOS**");
myValues.reverse();
document.write("<h1>Estos son los valores INVERTIDOS: </h1>");
document.write("<ul>");
for (const myValue of myValues) {
    document.write(`<li>${myValue}</li>`);
    console.log(myValue);
}
document.write("</ul>");

/* 5)   MOSTRAR CANTIDAD DE ELEMENTOS */
console.log("Cantidad de elementos:", myValues.length);
document.write(`<h2>La cantidad de elementos fueron: ${myValues.length}</h2>`);

/* 6)   BUSCAR UN ELEMENTO EN EL ARREGLO */
myInputVal = parseInt(prompt("Introduce el valor a buscar:"));
if(!isNaN(myInputVal)) {

    let mySearch = myValues.findIndex(mySearchedVal => mySearchedVal == myInputVal)

    if(mySearch != -1) {
        console.log("Indice del valor buscado: ", mySearch)
        alert(mySearch);
    }
    else {
        alert(`Lamentablemente, el valor ${myInputVal} no existe.`);
    }
}
else {
    alert("INFO: Por favor escribe un valor numérico.");
}
'use strict'
/* -------------- MANIPULAR TEXTOS-------------- */
let myNumber = 182;
let myStringA = "Texto de prueba";
let myStringB = ", estoy probando cadenas.";

// Convertir variable a Cadena
let myTempVar = myNumber.toString();
console.log(typeof myTempVar);

// Minusculas
myTempVar = myStringA.toLocaleLowerCase(); // Este metodo toma en cuenta el local del texto., a diferencia de toLowerCase
console.log(myTempVar);

// Mayusculas
myTempVar = myStringA.toLocaleUpperCase(); // Este metodo toma en cuenta el local del texto., a diferencia de toUpperCase
console.log(myTempVar);

// Cuenta de elementos
console.log(myTempVar.length);
myTempVar = ["Hola", "Mundo"];
console.log(myTempVar.length);

// Concatenar
myTempVar = myStringA + myStringB;      // Sin metodo
console.log(myTempVar);
myTempVar = myStringA.concat(myStringB);      // Con metodo
console.log(myTempVar);
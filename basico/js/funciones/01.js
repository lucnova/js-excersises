'use strict'
/* ------------------- FUNCIONES EN JS ------------------- */

function addValues(valA, valB, funcA, funcB) { // Se reciben como parametros (funciones anonimas)
    let result = valA + valB;

    funcA(result); // Callbacks
    funcB(result); // Callbacks

    return true;
}

addValues(
    2,
    2,
    function(data){
        console.log(data); // Cuando se hace el callback (sincrono)
    },
    function(data2){
        console.log(data2 * 2);
    }
);

'use strict'
/* ------------------- FUNCIONES EN JS - FLECHA ------------------- */

function addValues(valA, valB, funcA, funcB) { // Se reciben como parametros (funciones anonimas)
    let result = valA + valB;

    funcA(result); // Callbacks
    funcB(result); // Callbacks

    return true;
}

addValues(
    2,
    2,
    (data) => {
        console.log(data); // Tambien pueden escribirse asi las funciones anonimas, funcion flecha
    },
    (data2) => {
        console.log(data2 * 2);
    }
);

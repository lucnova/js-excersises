'use strict'
/* -------------- ÁMBITO DE LAS VARIABLES EN FUNCIONES -------------- */
function hello(myText) {
    let myLocalStr = "Muy buenas "; // Variable Local
    console.log(myText);    // Mostrando variable parametro
    console.log(myNum);     // Mostrando variable global

    console.log(myLocalStr);    // Mostrando variable local
}

let myNum = 12;
let myStr = "[VARIABLE GLOBAL] Hola, probando...";
hello(myStr);

console.log(myLocalStr);
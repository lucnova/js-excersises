'use strict'
/* ------------------- ALERTS E INGRESO DE DATOS ------------------- */

// Alertas Comunes
alert("Mensaje de alerta");

// Alertas de Confirmación
let myConfirm = confirm("Aceptas o Cancelas?");
console.log(myConfirm);


// Mensaje de Ingreso de datos
let myAge = prompt("Que edad tienes?", 18);
console.log("Edad: " + myAge + " - " + typeof myAge);
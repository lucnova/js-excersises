'use strict'
/* ------------------- TYPEOF ------------------- */
let myVarInt = 16;
let myVarFlt = 16.5;
let myVarStr = "16";
console.log(myVarInt + typeof myVarInt);
console.log(myVarFlt + typeof myVarFlt);
console.log(myVarStr + typeof myVarStr);
'use strict'
/* ------------------- LET Y VAR ------------------- */
/* ------------------- VAR ------------------- */
console.log("** PRUEBA CON VAR **");

var myNumVar = 32;
console.log("Valor Antiguo: " + myNumVar);

if(true) {
    var myNumVar = 20;
    console.log("Valor Nuevo DENTRO: " + myNumVar);
}

console.log("Valor Nuevo FUERA: " + myNumVar);


/* ------------------- LET ------------------- */
console.log("** PRUEBA CON LET **");
let myNumLet = 32;
console.log("Valor Antiguo: " + myNumLet);

if(true) {
    let myNumLet = 20;
    console.log("Valor Nuevo DENTRO: " + myNumLet);
}

console.log("Valor Nuevo FUERA: " + myNumLet);
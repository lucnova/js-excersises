'use strict'
/* ------------------- SWITCH ------------------- */
var myAge = 18;
var myMessage = "";

switch(myAge) {
    case 18:
        myMessage = "Acabas de ser mayor de edad.";
        break;
    default:
        myMessage = "Edad no contemplada.";
        break;
}

console.log(myMessage);
'use strict'
/* ------------------- OPERADORES Y TIPOS DE DATOS ------------------- */
let myVarA = 32;
let myVarB = 8;
let myVarR = myVarA / myVarB;

document.write("Operacion: " + myVarA + " % " + myVarB + " = " + myVarR);
document.write("<br>");

let myVarStr = "Te vi bailando";
document.write(myVarStr);
document.write("<br>");

myVarStr = "Te vi 'bailando'";
document.write(myVarStr);
document.write("<br>");

myVarStr = 'Te vi "bailando"';
document.write(myVarStr);
document.write("<br>");

/* -------------------------------------------------- */
myVarStr = "64";
console.log(myVarStr + " + 2  = " + (Number(myVarStr)+2)); // Convertir a cualquier numero

myVarStr = "64.5";
console.log(myVarStr + " + 2  = " + (parseInt(myVarStr)+2)); // Convertir a Entero

console.log(myVarStr + " + 2  = " + (parseFloat(myVarStr)+2)); // Convertir a Entero

myVarA = 16;
myVarStr = String(myVarA);
console.log("Anita lava la tina con " + myVarStr + " kilos de jabón");


console.log(String(12) + 3);
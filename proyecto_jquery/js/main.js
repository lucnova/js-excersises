'use strict';

/* --------------------------- MOSTRAR POST EN DOM -------------------------- */
function showPostOnDOM(postToShow, extraInfoCol) {
    /**
     * RECIBE DOS ARGUMENTOS: postToShow y extraInfoCol (opcional)
     *      postToShow => El objeto con datos "title, date, content" del post
     *                      a agregar.
     * 
     *      extraInfoCol => Argumento opcional. Representa el objeto con
     *                      información extra para la segunda columna.
     * 
     * 
     *      NOTA: Usaria jQuery pero no me gusta usarlo por lentitud; más que nada
     *              jQUery lo uso solo por Bootstrap.
     */
    //console.log(arguments.length);

    /* ELEMENTO ROW: Fila de datos que tendrá las 2 columnas */
    const rowDOM = document.createElement("div");
    rowDOM.className = "row mb-3";

    /* ELEMENTO COL: Primer columna de datos. Contendrá el post */
    const colDOM = document.createElement("div");
    colDOM.className = "col-8";

    /* ELEMENTO CARD: Elemento con datos del post en tipo carta */
    const cardDOM = document.createElement("div");
    cardDOM.className = "card mb-4";

    /* ELEMENTO CARD IMG: Imagen dentro de la carta */
    const cardImgDOM = document.createElement("img");
    cardImgDOM.className = "card-img-top";
    cardImgDOM.setAttribute("src", "http://placehold.it/750x300");
    cardImgDOM.setAttribute("alt", "Card image cap");

    /* ELEMENTO CARD BODY: Cuerpo de la carta/post */
    const cardBodyDOM = document.createElement("div");
    cardBodyDOM.className = "card-body";

    /* ELEMENTO H2 CARD: Titulo de la carta/post */
    const h2CardDOM = document.createElement("h2");
    h2CardDOM.className = "card-title";
    h2CardDOM.innerHTML = postToShow.title;

    /* ELEMENTO P CARD: Párrafo del post */
    const pCardDOM = document.createElement("p");
    pCardDOM.className = "card-text";
    pCardDOM.innerHTML = postToShow.content;

    /* ELEMENTO BTN CARD: Botón del post */
    const btnCardDOM = document.createElement("a");
    btnCardDOM.className = "btn btn-primary";
    btnCardDOM.innerHTML = "Read More &rarr;";

    /* ELEMENTO FOOTER CARD: Pie de la carta */
    const cardFooterDOM = document.createElement("div");
    cardFooterDOM.className = "card-footer text-muted";
    cardFooterDOM.innerHTML = `Posted on ${postToShow.date}`;

    /**
     *  ENCADENAR ELEMENTOS EN DOM.
     */
    /* 1)   Elementos del cuerpo de la carta */
    cardBodyDOM.appendChild(h2CardDOM);
    cardBodyDOM.appendChild(pCardDOM);
    cardBodyDOM.appendChild(btnCardDOM);
    /* 2)   Elementos de la carta */
    cardDOM.appendChild(cardImgDOM);
    cardDOM.appendChild(cardBodyDOM);
    cardDOM.appendChild(cardFooterDOM);
    /* 3)   Elementos contenedores (Columna y Fila) */
    //colDOM.appendChild(cardDOM);
    //rowDOM.appendChild(colDOM);

    // if (arguments.length === 2) {
    //     /* En caso de incluir info extra en la segunda columna */
    //     /* ELEMENTO COL INFO: Es el elemento columna, reperesenta la info extra al lado derecho */
    //     const colInfoDOM = document.createElement("div");
    //     colInfoDOM.className = "col-4";

    //     /* ELEMENTO H2 INFO: Título de la columna */
    //     const h2ColInfo = document.createElement("h2");
    //     h2ColInfo.className = "text-center";
    //     h2ColInfo.innerHTML = "Login";

    //     /* ELEMENTO P INFO: Contenido de la columna */
    //     const pColInfo = document.createElement("p");
    //     pColInfo.className = "text-justify";
    //     pColInfo.innerHTML = `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Velit doloremque nulla,
    //     perspiciatis provident dolore ducimus tenetur tempora voluptatibus, excepturi omnis deleniti
    //     a qui, animi aut. Possimus, eligendi? Sapiente, ipsam soluta.`;

    //     colInfoDOM.appendChild(h2ColInfo);
    //     colInfoDOM.appendChild(pColInfo);
    //     rowDOM.appendChild(colInfoDOM);
    // }

    /* Colocar elementos en postsListing */
    $("#postsListing").append(cardDOM);
}


$(document).ready(function () {
    /* ------------------------- REVISAR SI EXISTE TEMA ------------------------- */
    if (localStorage.getItem("theme") != undefined) {
        switch (localStorage.getItem("theme")) {
            case "blue":
                $("#theme").attr("href", "css/thm_blue.css");
                break;
            case "green":
                $("#theme").attr("href", "css/thm_green.css");
                break;
            case "red":
                $("#theme").attr("href", "css/thm_red.css");
                break;
        }
    }

    /* ------------------------------ REVISAR LOGIN ----------------------------- */
    if (localStorage.getItem("userLoginUser") != undefined) {
        const logoutBtn = document.createElement("button");
        logoutBtn.id = "logoutBtn";
        logoutBtn.className = "btn btn-primary";
        logoutBtn.innerHTML = "End Session";

        $("#userLoginForm").hide();
        $("#userLoginHeader").text("Welcome: @" + localStorage.getItem("userLoginUser"));

        $("#userLoginHeader").append(logoutBtn);

        $("#logoutBtn").click(function () {
            localStorage.removeItem("userLoginUser");
            location.reload();
        });
    }   

    /* ----------------------------- POSTS DINAMICOS ---------------------------- */
    const postsList = [];
    let aPost = {
        title: "Hello World",
        date: new Date(),
        content: "This is a webpage I made using Bootstrap + jQuery. I think it's pretty cool to learn about how to make them without using anything else."
    };

    postsList.push(aPost);

    aPost = {
        title: "Bread",
        date: new Date(),
        content: "Today I ate a really delicious, freshly baked bread from the local bakery... That's it."
    };

    postsList.push(aPost);

    /* ------------------------------ LISTAR EN DOM ----------------------------- */
    /* Usaría forEach o algo asi, pero no quiero desperdiciar memoria */
    const postCount = postsList.length; // Cuantos posts son

    // Llamo a esta funcion pues, ocupo mostrar una columna con info extra en la derecha
    //      (a diferencia de los demás posts que no necesitan esa columna)

    // EDIT 21-Ago-2020: Removi esto, quise que fuera separado el hecho de llamar 
    // esa columna a hacerse, y mejor hacerlo en HTML.
    //showPostOnDOM(postsList[0], "hi");

    // Iterar en la lista despúes del primer elemento
    for (let i = 0; i < postCount; i++) {
        showPostOnDOM(postsList[i]);
    }


    /* ---------------------------- BOTONES DE TEMAS ---------------------------- */
    $("#to-blue").click(function () {
        $("#theme").attr("href", "css/thm_blue.css");
        localStorage.setItem("theme", "blue");
    });

    $("#to-green").click(function () {
        $("#theme").attr("href", "css/thm_green.css");
        localStorage.setItem("theme", "green");
    });

    $("#to-red").click(function () {
        $("#theme").attr("href", "css/thm_red.css");
        localStorage.setItem("theme", "red");
    });

    /* ------------------------------ SCROLL TO TOP ----------------------------- */
    $("#scrollToTop").click(function () {
        $("html").animate({
            scrollTop: 0
        });
    });

    /* ---------------------------------- LOGIN --------------------------------- */
    $("#userLoginBtn").click(function (e) {
        localStorage.setItem("userLoginUser", $("#userLoginUser").val());
        //localStorage.setItem("userLoginPswd", $("#userLoginPswd").val());
    });
});